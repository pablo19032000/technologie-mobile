
const Liste_arrets = ({ data }) => {
    if (!data) {
      return <div>Chargement en cours ...</div>;
    }
    //else {
     //   var arrets=(data.records[0]).f

    
    return (
      <ol>
        {data.Countries.map((country) => (
          <li>
            {country.Country} : {country.TotalConfirmed}
          </li>
        ))}
      </ol>
    );
  };
  
  export default Liste_arrets;