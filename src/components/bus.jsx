import React, { useState, useEffect } from 'react';
import { getDevice }  from 'framework7/lite-bundle';
import {
  f7,
  f7ready,
  App,
  Panel,
  Views,
  View,
  Popup,
  Page,
  Navbar,
  Toolbar,
  NavRight,
  Link,
  Block,
  BlockTitle,
  LoginScreen,
  LoginScreenTitle,
  List,
  ListItem,
  ListInput,
  ListButton,
  BlockFooter,
  Subnavbar,
  Button,
  Segmented,
} from 'framework7-react';

import capacitorApp from '../js/capacitor-app';
import routes from '../js/routes';
import InputArret from './input_arret';
import Horaires from './horaires';
import InputLigne from './input_ligne';






const Bus = () => {

  const [totalArret, setTotalArret] = useState( {"":["list of bus lines"]});
  const [ligne,SetLigne]=useState("")
  const [arret,SetArret]=useState("")
  const [arretselect,SetArretselect]=useState("")
  const [ligneselect,SetLigneselect]=useState("")

 useEffect(() => {
   fetch("https://data.explore.star.fr/api/records/1.0/search/?dataset=tco-bus-topologie-dessertes-td&q=&rows=10000&facet=libellecourtparcours&facet=nomcourtligne&facet=nomarret")
     .then((rep) => {
       rep.json().then(rep => {
        var dict = {};
         for (let i=0 ; i< rep.records.length ; i++){
           let line= rep.records[i]
           
           if (!((Object.keys(dict))).includes(line["fields"]["nomarret"])){


            dict[line["fields"]["nomarret"]]=[line["fields"]["nomcourtligne"]];
           }
           else {
             if ( !( dict[line["fields"]["nomarret"]].includes(line["fields"]["nomcourtligne"])) ){

              dict[line["fields"]["nomarret"]].push(line["fields"]["nomcourtligne"]);
             }
             
           }
         }

        setTotalArret(dict); 
        

       })     
     })
    
 }, []);

  
  return (
    
    <Page style={style.appcontainer} onPtrRefresh={()=>{ 
      SetArretselect(""); 
      SetLigneselect("");
      document.getElementById("ar").innerHTML="";
      document.getElementById("li").innerHTML="";}}>
     <Navbar title="Horaires de Bus STAR" infinite={true} style={style.innerTitle} ></Navbar>
    
     <h2>choisis un arret</h2>
     <Segmented tag="p" raised>
      
        <input type="text" placeholder="arret" id="ar" onChange={(event) => {SetArret(event.target.value); }}/>
        <Button fill onClick={()=>{arret.length>0 ? SetArretselect(arret) : SetArret(""); SetArret(""); SetLigneselect(""); document.getElementById("li").innerHTML=" ";}}> Select </Button>
     </Segmented>

      <InputArret list_arrets={totalArret} inputarret={arret}/>

      <h2>choisis une ligne </h2>
      <Segmented tag="p" raised>
        
        <input type="text" width="50%" id="li" placeholder="ligne" onChange={(event) => {SetLigne(event.target.value)}}/>

        <Button fill onClick={()=>{ ligne.length>0 ? SetLigneselect(ligne): SetLigne("") ; SetLigne("");}}> Check </Button>
        
     </Segmented>

      <InputLigne allLigne={totalArret[arretselect]} input={ligne}/>
     
     
      <h3 style={style.innerText}>Station :  {arretselect}</h3>
      <h3 style={style.innerText}>  Ligne : {ligneselect} </h3>

    <Horaires arret={arretselect} ligne ={ligneselect}/>
    

    </Page>        
    
    

    
  )
        
}
export default Bus;



const style={
  appcontainer:{
    flex: 1,
    backgroundColor:"#E5E7E9",
    alignItems: "center",
    justifyContent: "center",
    padding : 15

  },
  innerText: {
    color: 'red'
  },
  innerTitle: {
    color: 'blue'
  },
  inputcontainer1 :{
    flexDirection: 'row',
    justifyContent : 'space-between',
    marginRight:9,
    padding:8,
    paddingBottom:10,
    alignItems : 'center'

  },
  inputcontainer2 :{
    flexDirection: 'row',
    justifyContent : 'space-between',
    marginRight:9,
    padding:8,
    alignItems : 'center',
    paddingBottom:10,
    borderEndWidth:1,
    borderBottomColor:'yellow',

  },
  horairescontainer:{
    flex:3,

  }
}