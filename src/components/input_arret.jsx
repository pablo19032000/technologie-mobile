

import React from 'react';
import {
  Page,
  Navbar,
  Subnavbar,
  Searchbar,
  List,
  ListItem,
  theme,
} from 'framework7-react';

 const InputArret =({list_arrets, inputarret}) => {

    if (!list_arrets) {
        return <div>Chargement en cours ...</div>;  }
    else {    

      if (Object.keys(list_arrets).length>0){ 
        return(
        <div>
          
          {Object.keys(list_arrets).filter(elt =>elt.toLowerCase().includes(inputarret.toLowerCase()) & inputarret.length>2 ).map((val,key) => { 
            return ( 

              <div key={val}>  {val}   </div>); })} 
        
        </div>
        )
        
      }
      else {
        return ( <div> </div>)
      }
  } };

 export default InputArret ;