import React, { useEffect, useState } from "react";
import {
  Page,
  Navbar,
  Subnavbar,
  Searchbar,
  List,
  ListItem,
  theme,
  Segmented
} from 'framework7-react';

 const Horaires =({arret, ligne , n}) => {
     const left="https://data.explore.star.fr/api/records/1.0/search/?dataset=tco-bus-circulation-passages-tr&rows=10000&sort=-departtheorique&timezone=Europe%2FParis&refine.nomarret="
     const midle="&refine.nomcourtligne="
     var now = new Date();
     var annee   = now.getFullYear();
     var mois    = now.getMonth() + 1;
     var jour    = now.getDate();
     var heures   = now.getHours();
     var minute  = now.getMinutes();
      console.log(mois)
    const [heure, setHeure] = useState();
    useEffect(() => {
    const url=left.concat('',arret).concat('',midle).concat('',ligne);
    fetch(url)
    .then((rep) => {
        rep.json().then(rep => {
         var dict =[]
         for (let i=0; i< rep.records.length; i++){
           if (i>=0){
            dict.push([rep.records[i]["fields"]["destination"],rep.records[i]["fields"]["depart"].split('T')[1].substr(0, 5)]);
           }
         }
         setHeure(dict)

        })     
    })
   
}, [arret,ligne,n]);
     
if (!heure) {
  return <div>...</div>;  }
else { 
  if (heure.length>0){
    return (
      <div>
     <h3> Le {jour}/{mois}/{annee} ; il est {heures} heure {minute} minutes  </h3> 
     <h2> Directions {"...."}   horaires</h2>
    <List className="times">
      {
        heure.slice(0,n+1).map((h, i) => {
          return (
            <Segmented>
              <div> {h[0]}</div>
              <div> {"...."} </div>
              <div> {h[1]}</div>
            
            </Segmented>
          )
          
        })
      }
    </List>
    
    </div>
    
  )
    
  }
  else {
    return (
      <div>
     <h3> Le {jour}/{mois}/{annee} ; il est {heures} heure {minute} minutes  </h3> 
     <h2> Directions {"...."}   horaires</h2>
     <div> horaires indisponibles </div>
     </div>
     
      )}
  }
  
  
 } ;

 export default Horaires ;