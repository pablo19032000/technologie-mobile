

import React, { useState, useEffect } from 'react';
import {
  Page,
  Navbar,
  Subnavbar,
  Searchbar,
  List,
  ListItem,
  theme,
} from 'framework7-react';
import Horaires from './horaires';

 const InputLigne =({allLigne, input}) => {

    if (!allLigne) {
        return <div>waiting...</div>;  
    }                  
        
    else {
      if (allLigne.length>0 ) {  
      
        return (
          <div className='ligne'>
            { 
              allLigne.filter(elt => elt.toLowerCase().includes(input.toLowerCase()) & input.length>0 ).map((val,key) => {
                return (
                  <div className='lines' key={val}>  {val} </div>
  
                )
              })
            }
   
          </div>
  
        )
    
      }
  
      else{
        return (
          <div>
          </div>
        )
      }

    }
    
 }

      
 export default InputLigne ;